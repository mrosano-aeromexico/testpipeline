package com.mx.aeromexico;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExampleName {
	
	private String clazz;
	
	public void setclazz(String clazz) {
		this.clazz = clazz;
	}

	public String getclazz() {
		return clazz;
	}
	
	private static final String REGEX_RFC = "^([A-Z]{4}([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1]))([A-Z0-9]{3})$";
	
	public static void main(String[] args) {
		String rfc = "ROPM850427FK1";
		String prueba = "aaaaaaaaaaaaaaaaaaaaaaaaaaaaabs";
		System.out.println("length: " + prueba.length());
		
		boolean bandera = false;
		boolean banderaDos = false;
		bandera = validarRfc(rfc);
		
		banderaDos = validateRfc(rfc);
		
		String validRfc = rfc.trim();
		
		if (validRfc.length() >= 7000) {
		    throw new Error(" " + rfc + " not a valid Decimal128 string");
		}
		
		boolean stringMatch = validRfc.matches(REGEX_RFC);
		
		
		boolean banderaTres = Pattern.matches(REGEX_RFC, rfc);
		
		
		System.out.println("bandera_rfc: " + bandera);
		//System.out.println("bandera_dos_rfc: " + banderaDos);
		//System.out.println("bandera_tres_rfc: " + banderaTres);
		//System.out.println("bandera_cuatro_rfc: " +stringMatch);
	}
	
	public static boolean validarRfc(String rfc){
		rfc = rfc.toUpperCase().trim();
		return rfc.toUpperCase().matches("[A-Z]{4}[0-9]{6}[A-Z0-9]{3}");
	}//Cierra método validarRFC
	
	public static boolean validateRfc(String rfc) {
		Matcher rfcMatcher = null;
		Pattern rfcPattern = null;
    	if(!REGEX_RFC.isEmpty()) {
			rfcPattern = Pattern.compile(REGEX_RFC);
	    	rfcMatcher = rfcPattern.matcher(rfc);
    	}
    	return rfcMatcher.matches();
    }

}

