package com.mx.aeromexico;

public class NumberFormat {
	
	public static void main(String[] args) {
		/*String name = "";
		try{
		    int i = Integer.parseInt(name);
		    System.out.println("Resultado: " + i);
		} catch(NumberFormatException ex){ // handle your exception
		    System.out.println("message: " + ex.getMessage());
		    ex.printStackTrace();
		}*/
		
		String[] people = new String[] { "Carol", "Andy" };

		// An array will be created:
		// people[0]: "Carol"
		// people[1]: "Andy"
		
		int nums[]=new int[4];

        try {
            System.out.println("Antes de que se genere la excepción.");
            //generar una excepción de índice fuera de límites
            nums[7]=10;
        }catch (ArrayIndexOutOfBoundsException exc){
            //Capturando la excepción
            System.out.println("Índice fuera de los límites!");
        }
        System.out.println("Después de que se genere la excepción.");

		// Notice: no item on index 2. Trying to access it triggers the exception:
		try {
			
			System.out.println(people[3]);  // throws an ArrayIndexOutOfBoundsException.
			
		}catch (ArrayIndexOutOfBoundsException ai) {
			 ai.printStackTrace();
		}
		
		
		
	}

}
